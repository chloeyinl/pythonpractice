# prompt user for hours and rate per hour to compute gross pay
hrs = input("Enter Hours: ")
rate = input("Enter Rate: ")

try:
    fh = float(hrs)
    fr = float(rate)
except:
    print("Error, please enter numeric input")
    quit()

if fh > 40:
    pay = (40 * fr) + (fh - 40) * fr * 1.5
else:
    pay = fh * fr
print(pay)




